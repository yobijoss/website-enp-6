var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
var validator = require('express-validator');

var mongoose = require('mongoose');
var model = require('./src/model/schemas')(mongoose);

var app = express();
var port = 5000;

//some things we want to create

//lets create our routes
var mainRouter = require('./src/routes/mainRoute')();
var messageRouter = require('./src/routes/messagesRoute')(model);
var eventsRouter = require('./src/routes/eventsRoute')(model);
var authRouter = require('./src/routes/authRoute')();
var apiRoute = require('./src/routes/apiRoute')(model);
var diffusionRoute = require('./src/routes/diffusionRoute')(model);


// only for testing
var testRoute = require('./src/routes/testRoute')(model);


app.use(express.static('public'));

//what are we going to use
app.use(flash());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(validator({
	customValidators:{
		isUrl:function(value){
			//regex by stackoverflow user: Rorshack
			return /^((https?):\/\/)([w|W]{3}\.)+[a-zA-Z0-9\-\.]{3,}\.[a-zA-Z]{2,}(\.[a-zA-Z]{2,})?$/.test(value);
		}
	}
}));
app.use(cookieParser());
app.use(session({secret: 'library',resave : false}));
//config passport
require('./src/config/passport')(app,model);



// views is directory for all template files
app.set('views','src/views');
app.set('view engine', 'ejs');

//here we define routing

app.use('/auth',authRouter);
app.use('/api',apiRoute);
app.use('/mensajes',messageRouter);
app.use('/eventos',eventsRouter);
app.use('/NuevaDifusion',diffusionRoute);
//only for testing
app.use('/test',testRoute);
app.use('/',mainRouter);


//Lets listen
app.listen(port, function() {
  
    console.log('Node app is running on port', port);
});


