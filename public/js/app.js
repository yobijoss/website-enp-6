$(function(){
    Interface.init();

    

    $('#btn-submit-message').click(function(){
            $('#msgForm').submit();
    });
    $('#btn-submit-event').click(function(){
        var date = $('#datetimepicker1').data("DateTimePicker").date();
        $('input[name=eventDate]').val(date)
        $('#eventForm').submit();
    });

    $('#datetimepicker1').datetimepicker({
            sideBySide: true,
            locale : 'es',
            format : 'DD/MM/YYYY hh:mm a'   
        });

    
});

function logOut(){
    window.location.href = "/api/logout";
}

var Interface = function(){
    var _this;
    return {
        init: function() {
            _this = this;
			this.initJscroll();
        },
        
		initJscroll: function() {
			$('.scroll').jscroll({
                loadingHtml: '',
            });
		}
    };
}();
