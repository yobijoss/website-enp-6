# Applicación para Mensajería de la Escuela Nacional Preparatoria 6 "Antonio Caso"


Aplicación Web desarrollada utilizando [Express 4](http://expressjs.com/).


## Deploy Local

Asegurate de tener [Node.js](http://nodejs.org/) y  [Heroku Toolbelt](https://toolbelt.heroku.com/) Instalados.

```sh
$ git clone https://bitbucket.org/dev_enp6/app-enp-6-web # 
$ cd app-enp-6-web
$ npm install
$ npm start
```

La aplicación debería de estar corriendo en  [localhost:5000](http://localhost:5000/).

## Deploy en Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentacion

Más información acerca de Nodejs y Heroku:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)


## Hecho para la Universidad Nacional Autónoma de México

- [Universidad Nacional Autónoma de México](https://www.unam.mx)
- [Dirección General de la Escuela Nacional Preparatoria](http://dgenp.unam.mx)
- [Escuela Nacional Preparatoria #6 "Antonio Caso"](http://www.prepa6.unam.mx/ENP6/_P6/)
