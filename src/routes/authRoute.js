var express = require('express');
var authRouter  = express.Router();

var router = function() {

    authRouter.route('/')
        .get(function(req, res) {
            if(!req.user){
                
                var errorMesage = req.flash('error')[0];

                res.render('pages/auth',{
                    user: req.user,
                    errorMessage: errorMesage
                });
                
            }else{
                res.redirect('/mensajes');
            }
        });
    
    return authRouter;
};

module.exports = router;
