var express = require('express');
var testRouter  = express.Router();
var mongoose = require('mongoose');


var router = function(model) {

    testRouter.route('/')
        .get(function(req, res) {
        mongoose.connect('mongodb://localhost:27017/appenplocal');

        var db = mongoose.connection;

        db.on('error',console.error.bind(console,'connection error: '));
        db.once('open', function() {
           console.log('WE ARE CONNECTED :D');
        });
        
        for(var i = 0 ; i< 50 ; i++){
            var testMessage = new model.Message({
                title : 'title '+i,
                body : 'body'+i,
                link : 'www.google.com',
                imageUrls : [{link : 'www.link'+i+'.com'},{link : 'www.link1.com'}]
            });
            
            testMessage.save(function(err,testMessage){
                if(err){
                    return console.error(err);
                }
                console.log('object save '+testMessage);
            });
        }
        
       
    });
    
   
    return testRouter;
};

module.exports = router;