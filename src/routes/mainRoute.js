var express = require('express');
var mainRouter  = express.Router();

var router = function() {
    
    mainRouter.route('/')
        .get(function(req, res) {
            if(req.user){
                res.redirect('/mensajes');
            }
            else{
                res.render('pages/main',{user: req.user});
            }
        });
    
    return mainRouter;
};

module.exports = router;
