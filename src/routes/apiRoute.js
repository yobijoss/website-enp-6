var express = require('express');
var apiRouter = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');

var router = function (model) {
    
    apiRouter.route('/login')
        .post(passport.authenticate('local',{
            failureRedirect: '/auth',
            successRedirect: '/mensajes',
            failureFlash: true,
            badRequestMessage: 'Campos Vacios'
    }));
    
    apiRouter.route('/register')
        .post(function (req, res) {
            
            //validate data please
            
            req.checkBody('userName','Hay Campos Vacios').notEmpty();
            req.checkBody('password','Hay Campos Vacios').notEmpty();
            req.checkBody('email','Hay Campos Vacios').notEmpty();
            req.checkBody('id','Hay Campos Vacios').notEmpty();
            req.checkBody('email','Correo Invalido').isEmail();
            req.checkBody('password','Las Contraseñas no Coinciden').equals(req.body.passwordConfirm);

            var errors = req.validationErrors();
            if (errors) {
                   req.flash('error', errors[0].msg);
                   res.redirect('/auth');
                return;
            }
            
            mongoose.connect('mongodb://localhost:27017/appenplocal');
            
            var userAdded = new model.User({
                    username: req.body.userName,
                    password: req.body.password,
                    email: req.body.email,
                    id: req.body.id
            });
            
            model.User.find().byId(userAdded.id).exec(function(err,user){
                if (err) {
                    console.error('There is an error, going to auth '+err);
                    mongoose.disconnect();
                    res.redirect('/auth');
                    
                }else {
                    if (!user) {
                        console.log('Lets save the user '+userAdded.dudify());
                        
                        userAdded.save(function(err){
                            if (err){throw err;} 

                             req.login(userAdded, function () {
                                res.redirect('/mensajes');
                            });
                            mongoose.disconnect();
                        });
                    } else{
                        mongoose.disconnect();
                        res.redirect('/auth');
                    }
                }
            });
    });

    //route for user when sends a message from the
    //frontend
    apiRouter.route('/sendMessage')
        .post(function (req, res) {
            
            //validate data please
            
            req.checkBody('title','No Hay Titulo').notEmpty();
            if (req.body.url) {
                req.checkBody('url','URL NO VALIDA').isUrl();
            }
            
            req.checkBody('content','Escribe el mensaje').notEmpty();
            req.checkBody('users','No haz seleccionado Usuarios').notEmpty();

            var errors = req.validationErrors();

            if (errors) {
                   req.flash('errormsg', errors[0].msg);
                   res.redirect('/NuevaDifusion');
                return;
            }
            
            mongoose.connect('mongodb://localhost:27017/appenplocal');
            
            var msg = new model.Message({
                    title: req.body.title,
                    content: req.body.content,
                    scope: req.body.users,
                    idUser: req.user._id,
                    url: req.body.url,
                    urlImage : req.body.urlImage
            });

           msg.save(function(err){
                if (err){
                    req.flash('errormsg', 'Error al procesar el mensaje');
                    res.redirect('/NuevaDifusion');
                    throw err;
                } 
                mongoose.disconnect();
                res.redirect('/mensajes');
            });
        
    });

        //frontend
    apiRouter.route('/sendEvent')
        .post(function (req, res) {
            
            //validate data please
            req.checkBody('title','No Hay Titulo').notEmpty();

            if (req.body.url) {
                req.checkBody('url','URL NO VALIDA').isUrl();
            }

            req.checkBody('content','Escribe el mensaje').notEmpty();
            req.checkBody('academicNum','Número de asistentes alumnos vacio').notEmpty();
            req.checkBody('academicNum','Número de asistentes alumnos invalido').isInt();
            req.checkBody('schoolarsNum','Número de asistentes academicos vacio').notEmpty();
            req.checkBody('schoolarsNum','Número de asistentes academicos invalido').isInt();
            req.checkBody('users','No haz seleccionado Usuarios').notEmpty();
            req.checkBody('eventDate','No haz seleccionado una fecha').notEmpty();


            var errors = req.validationErrors();

            if (errors) {
                   req.flash('errorevt', errors[0].msg);
                   res.redirect('/NuevaDifusion');
                return;
            }
            
            mongoose.connect('mongodb://localhost:27017/appenplocal');
            console.log("date value "+req.body.eventDate)

            var event = new model.Event({
                    title: req.body.title,
                    content: req.body.content,
                    scope: req.body.users,
                    idUser: req.user._id,
                    url: req.body.url,
                    urlImage : req.body.urlImage,
                    academicNum: req.body.academicNum,
                    schoolarsNum: req.body.schoolarsNum,
                    eventDate : new Date(req.body.eventDate),
            });

           event.save(function(err){
                if (err){
                    console.error("The error is "+err);
                    req.flash('errorevt', 'Error al procesar el evento');
                    res.redirect('/NuevaDifusion');
                } 
                mongoose.disconnect();
                res.redirect('/eventos');
            });
        
    });
    
    apiRouter.route('/logout')
        .get(function(req, res){
            req.logout();
            req.session.destroy();
            res.redirect('/');
        });
    
    return apiRouter;
};

module.exports = router;