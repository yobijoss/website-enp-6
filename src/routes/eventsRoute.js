var express = require('express');
var mainRouter  = express.Router();
var mongoose = require('mongoose');

var router = function(model) {

    mainRouter.use(function(req,res,next){
        if(!req.user){
            res.redirect('/');
        }
        else{
            next();
        }
    });
    
    mainRouter.route('/')
        .get(function(req, res) {
             mongoose.connect('mongodb://localhost:27017/appenplocal');

            var db = mongoose.connection;

            db.on('error',console.error.bind(console,'connection error: '));
        
            model.Event.find().byPagination(0).exec(function (err, events) {
                if (!err) {
                    res.render('pages/events',{
                        user: req.user,
                        sidebar: 2,
                        eventArray: events,
                        nextPage:1});
                }else{
                    res.json({'error ': err});
                }
                mongoose.disconnect();
            });  
        });

    //way for out infinite scroll
    mainRouter.route('/:page')
        .get(function(req, res) {

            var pagination = req.params.page * 10;
            var nextPage = parseInt(req.params.page) +1;
            mongoose.connect('mongodb://localhost:27017/appenplocal');

            var db = mongoose.connection;

            db.on('error',console.error.bind(console,'connection error: '));
        
            model.Event.find().byPagination(pagination).exec(function (err, events) {
                 if (!err) {
                    console.log("this are the events "+events)
                    if (events) {
                        res.render('partials/eventsArray',{eventArray: events,nextPage:nextPage});
                    } 
                }else{
                    console.error(err)
                    res.render('partials/eventsArray',{eventArray: events,nextPage:nextPage});
                }
                mongoose.disconnect();

            });
    });
            
    
    return mainRouter;
};

module.exports = router;