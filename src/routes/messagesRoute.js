var express = require('express');
var mainRouter  = express.Router();
var mongoose = require('mongoose');

var router = function(model) {

    mainRouter.use(function(req,res,next) {
        if (!req.user) {
            res.redirect('/');
        }
        else {
            next();
        }
    });

    mainRouter.route('/')
        .get(function(req, res) {
        mongoose.connect('mongodb://localhost:27017/appenplocal');

        var db = mongoose.connection;

        db.on('error',console.error.bind(console,'connection error: '));
        
        model.Message.find().byPagination(0).exec(function (err, messages) {
            if (!err) {
                res.render('pages/messages',{
                    user: req.user,
                    sidebar: 1,
                    messages: messages,
                    nextPage:1});
            }else{
                res.json({'error ': err});
            }
             mongoose.disconnect();
        });        
    });
    
    //This Route will retreive message in a paginated 
    //way for out infinite scroll
    mainRouter.route('/:page')
        .get(function(req, res) {

            var pagination = req.params.page * 10;
            var nextPage = parseInt(req.params.page) +1;
            mongoose.connect('mongodb://localhost:27017/appenplocal');

            var db = mongoose.connection;

            db.on('error',console.error.bind(console,'connection error: '));
        
            model.Message.find().byPagination(pagination).exec(function (err, messages) {
                 if (!err) {
                    if (messages) {
                        res.render('partials/messageArray',{messages: messages,nextPage:nextPage});
                    } 
                }else{
                     res.render('partials/messageArray',{messages: messages,nextPage:nextPage});
                }
                mongoose.disconnect();

            });
            
        });


    return mainRouter;
};

module.exports = router;