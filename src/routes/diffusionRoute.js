var express = require('express');
var diffusionRouter  = express.Router();

var router = function(model) {

    diffusionRouter.use(function(req,res,next){
        if(!req.user){
            res.redirect('/');
        }
        else{
            next();
        }
    });
    
    diffusionRouter.route('/')
        .get(function(req, res) {

            var errormsg = req.flash('errormsg')[0];
            var errorevt = req.flash('errorevt')[0];

            var whichShouldShow = 1;
            
            if (errorevt) {whichShouldShow = 2}
                
            res.render('pages/difusion', {
                    user: req.user,
                    sidebar: 3,
                    errorMessage : errormsg,
                    errorEvent : errorevt,
                    tabToShow : whichShouldShow
                });
        });
    
    return diffusionRouter;
};

module.exports = router;