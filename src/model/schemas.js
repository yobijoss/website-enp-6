module.exports = function(mongoose){
    
    var Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId;
    
    //Messages 
    var messageSchema = new Schema({
        title : String,
        content : String,
        scope: [String],
        idUser: ObjectId,
        url : {type : String, default:'http://www.prepa6.unam.mx/ENP6/_P6/'},
        urlImage : String,
        createdDate : {type : Date, default: Date.now}
    });
   
    
    //Events
    var eventSchema = new Schema({
        title : String,
        content : String,
        scope: [String],
        idUser: ObjectId,
        url : {type : String, default:'http://www.prepa6.unam.mx/ENP6/_P6/'},
        urlImage : String,
        createdDate : {type : Date, default: Date.now},
        academicNum: Number,
        schoolarsNum: Number,
        eventDate : {type : Date}
    });
    
    
    //User
    var userSchema = new Schema({
        username : String,
        email : String,
        id : String,
        createdAt : {type : Date, default: Date.now},
        permission : Number,
        password : String,
        enabled : Boolean
     });
   
    //built in queries
    userSchema.query.byId = function(id){
        return this.findOne({id : id});
    };


    messageSchema.query.byPagination = function(page){
        return this.find({}).skip(page).sort({createdDate : -1}).limit(10);
    };


    eventSchema.query.byPagination = function(page){
        return this.find({}).skip(page).sort({createdDate : -1}).limit(10);
    };
        
    //Return the Model
    var model = {
        Message : mongoose.model('Message',messageSchema),
        User : mongoose.model('User',userSchema),
        Event : mongoose.model('Event',eventSchema)
    };
    
    return model;
};