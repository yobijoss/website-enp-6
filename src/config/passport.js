var passport = require('passport');

//lets only configure passport

module.exports = function(app,model) {
    app.use(passport.initialize());
    app.use(passport.session());

    // adds an retreives user to the session (when we call log in!)
    passport.serializeUser(function(user,done) {
        // this is used to save a user
        done(null,user); // IN FACT IS user.id or something to recognize the user
    });

    // when log out?
    passport.deserializeUser(function(user,done) {
        // this is used to obtain the user
        // userId to obtain the user mongo.find({id: user.id})
        done(null,user);
    });

    require('./strategies/local.strategy')(model);

};