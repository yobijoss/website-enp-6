var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    url = 'mongodb://localhost:27017/appenplocal';

var mongoose = require('mongoose');

module.exports  = function(model) {
    passport.use(new LocalStrategy({
        usernameField: 'id',
        passwordField: 'password'
    },
    function(id,password,done) {

        // form is okay? lets connect to database
        mongoose.connect(url);

        model.User.find().byId(id).exec(function(err,user) {
            mongoose.disconnect();
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null,false,{message: 'Usuario no encontrado.'});
            }
            if (user.password !== password) {
                return done(null,false,{message: 'Contraseña Incorrecta.'});
            }

            return done(null,user);
        });
    }));
};